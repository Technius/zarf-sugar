
######################################################
# Constructors
######################################################

data List[a] = Cons a List[a]
     	       | Nil

data Maybe[a] = Just a
     	      | Nothing

data Tuple[a,b] = Tuple a b

data Tuple3[a,b,c] = Tuple3 a b c

data Tuple4[a,b,c,d] = Tuple4 a b c d

data BTNode[a] = BTNode a BTNode[a] BTNode[a]
     	         | BTNodeNil

data BTArray[a]
  = BTArray Int a BTArray[a] BTArray[a]
  | BTArrayNil

data Queue[a] = Queue List[a] List[a]

data IntMap[a] = IntMap Int Int IntMap[a] IntMap[a]
     	   | IntMapLeaf Int a
	   | IntMapEmpty

######################################################
# Random Helpers
######################################################
# invertBits
# byte
# nibble
# min
# max
# pow
# not
# mod

# Assemble a 32-bit value from two 16-bit constants
fun make32 :: (Int, Int) -> Int
fun make32 hi lo =
  let hival = sll hi 16 in
  or hival lo

fun invertBits :: (Int) -> Int
fun invertBits x =
  let upper = sll 65535 16 in
  let ones = or upper 65535 in
  xor ones x

# Gets the nth byte of the 32-bit number x
# getbyte :: Int -> Int -> Int
fun getbyte :: (Int, Int) -> Int
fun getbyte n x =
  let oob = leq 4 n in
  case oob of {
  0 =>
    let shiftAmount = sll n 3 in
    let m1 = sll 255 shiftAmount in
    srl (and m1 x) shiftAmount,
  _ => Error
}

# Gets the nth nibble of the 32-bit number x
# nibble :: Int -> Int -> Int
fun nibble :: (Int, Int) -> Int
fun nibble n x =
  let oob = leq 8 n in
  case oob of {
    0 =>
      let shiftAmount = sll n 2 in
      let m1 = sll 15 shiftAmount in
      srl (and m1 x) shiftAmount,
    _ => Error
  }

# set byte # 'byteNum' to 'byte' in 32-bit number 'num'
# num = [byte3, byte2, byte1, byte0]
fun setByte :: (Int, Int, Int) -> Int
fun setByte byteNum byte num =
  let shiftAmount = sll byteNum 3 in
  let m1 = sll 255 shiftAmount in
  let mask = not m1 in
  let b1 = and mask num in
  let byte2 = sll byte shiftAmount in
  let b2 = or b1 byte2 in
  b2

fun min :: (Int, Int) -> Int
fun min x y =
  let cond = lt x y in
  case cond of {
    0 => y,
    1 => x,
    _ => Error
  }

fun max :: (Int, Int) -> Int
fun max x y =
  let cond = lt x y in
  case cond of {
    0 => x,
    1 => y,
    _ => Error
  }

# pow :: Int -> Int -> Int
# a ^ b
# I case on a first to be a little more efficient
fun pow :: (Int, Int) -> Int
fun pow a b =
  case a of {
    0 => case b of {
           0 => Error,
           _ => 0
         },
    1 => 1,
    _ =>
      case b of {
        0 => 1,
        _ => mult a (pow a (b - 1))
      }
  }

fun not :: (Int) -> Int
fun not x = nand x x

# 0x110
fun mod :: (Int, Int) -> Int
fun mod x y =
  case y of {
    0 => Error,
    _ =>
      let dv = div x y in
      sub x (y * (x / y))
  }

######################################################
# Output operations
######################################################

fun intToString :: (Int) -> List[Int]
fun intToString val =
  let end = Nil in
  case val of {
    0 => Cons 48 end,
    _ =>
      intToStringRec val end
  }

fun intToStringRec :: (Int, List[Int]) -> List[Int]
fun intToStringRec val str =
  case val of {
    0 => str,
    _ =>
      let {
        newval = val / 10,
        tmp = newval * 10,
        digit = val - tmp,
        code = 48 + digit,
        newstr = Cons code str
      } in
      intToStringRec newval newstr
  }

fun print_int :: (Int) -> Int
fun print_int val =
  let {
    end = Nil,
    ltail = Cons 10 end, # newline character
    s = intToStringRec val ltail
  } in
  print_int_list s

# Append val to the front of list until it reaches length len
fun lfill :: (List[a], Int, a) -> List[a]
fun lfill list len val =
  lfillrec list (len - (length list)) val

# 0x115
fun lfillrec :: (List[a], Int, a) -> List[a]
fun lfillrec list n val =
  let done = lt n 1 in
  case done of {
    1 => list,
    0 => lfillrec (Cons val list) (n - 1) val,
    _ => Error
  }

######################################################
# Input operations
######################################################

# Read a string (of bytes) from stdin until newline or EOF encountered
fun readline :: List[Int]
fun readline =
  let reversed = readlinerec Nil in
  reverse reversed

fun readlinerec :: (List[Int]) -> List[Int]
fun readlinerec l =
  let val = getint 0 in
  case val of {
    0x100 => l,        # EOF
    10 => Cons val l,  # newline
    _ => readlinerec (Cons val l)
  }

# Read a line of input and parse an integer. Ignore all non-digit characters.
fun readint :: Maybe[Int]
fun readint =
  let line = readline in
  findint line

# Read through a string, skipping non-numeric characters.
# When a digit is found, call parseint.
fun findint :: (List[Int]) -> Maybe[Int]
fun findint line =
  case line of {
    Nil => Nothing,
    Cons x xs =>
      let {
        under = lt x 48,  # ascii codes 48-57 correspond to ints
        over = lt 57 x
      } in
      case under of {
        1 => findint xs,
        0 =>
          case over of {
            1 => findint xs,
            0 =>
              let newval = sub x 48 in
              parseintrec xs newval,
            _ => Error
          },
        _ => Error
      }
  }

# Parse an integer from the given string. Stop when non-digit encountered.
fun parseint :: (List[Int]) -> Maybe[Int]
fun parseint line = parseintrec line 0

# Recursively read chars, formulating value of int
fun parseintrec :: (List[Int], Int) -> Maybe[Int]
fun parseintrec line val =
  case line of {
    Nil => Just val,
    Cons x xs =>
      let {
        under = lt x 48, # ascii codes 48-57 correspond to ints
        over = lt 57 x
      } in 
      case under of {
        1 => Just val,
        0 =>
          case over of {
            0 =>
              let {
                shifted = val * 10,
                charval = x - 48, # value of integer digit
                newval = shifted + charval
              } in
              parseintrec xs newval,
            _ => Just val
          }
        _ => Error
      }    
  }

######################################################
# List operations
######################################################
# Defined here:
# map
# head
# tail
# print_list
# eval_list
# concat
# flip
# compose
# filter
# take
# drop
# length
# split
# foldr
# foldr
# foldl
# zipWith
# nth
# slice
# repeat
# append
# is_empty
# updated
# print_list_p
# qsort
# qsort_int

fun map :: ((a) -> b, List[a]) -> List[b]
fun map f l =
  case l of {
    Nil => Nil,
    Cons x xs => Cons (f x) (map f xs)
  }

fun head :: (List[a]) -> a
fun head l =
  case l of {
    Cons x xs => x,
    Nil => Error
  }

fun tail :: (List[a]) -> List[a]
fun tail l =
  case l of {
    Nil => l,
    Cons x xs => xs
  }

fun print_int_list :: (List[Int]) -> Int
fun print_int_list l =
  case l of {
    Nil => 0,
    Cons x xs =>
      let out = putint 0 x in
  	  let rec = print_int_list xs in
  	  case out of {
        _ => rec
      }
  }

# 0x11a
fun eval_list :: (List[a]) -> Int
fun eval_list l = 0
#case l of {
#  Nil => 0
#  Cons x xs => let rec = eval_list xs in
#       	       case x of { _ => rec }
#}

fun concatAll :: (List[List[a]]) -> List[a]
fun concatAll l =
  case l of {
    Nil => Nil,
    Cons h tl => concat h (concatAll tl)
  }

# concat :: [a] -> [a] -> [a]
fun concat :: (List[a], List[a]) -> List[a]
fun concat x y =
  case x of {
    Nil => y,
    Cons a as => Cons a (concat as y)
  }

# flip : (a -> b -> c) -> b -> a -> c
fun flip :: ((a, b) -> c, b, a) -> c
fun flip f a b = f b a

# (.) : (b -> c) -> (a -> b) -> a -> c
fun compose :: ((b) -> c, (a) -> b, a) -> c
fun compose f g a = f (g a)

fun compose2 :: ((c) -> d, (a, b) -> c, a, b) -> d
fun compose2 f g x y = f (g x y)

# filter : [a] -> (a -> bool) -> [a]
fun filter :: (List[a], (a) -> Int) -> List[a]
fun filter l f =
  case l of {
    Nil => l,
    Cons x xs =>
      let {
        test = f x,
        ltail = filter xs f
      } in
      case test of {
        0 => ltail,
        1 => Cons x ltail,
        _ => Error
      }
  }

# Take first n elements from list L
fun take :: (Int, List[a]) -> List[a]
fun take n l =
  case l of {
    Nil => l,
    Cons x xs =>
      case n of {
        0 => Nil,
        _ => Cons x (take (n - 1) xs)
      }
  }

# 0x120
# (a -> Bool) -> [a] -> [a]
# Get longest prefix from L that satisfies predicate f
fun takeWhile :: ((a) -> Int, List[a]) -> List[a]
fun takeWhile f l =
  case l of {
    Nil => l,
    Cons x xs =>
      let continue = f x in
      case continue of {
        0 => Nil,
        1 => Cons x (takeWhile f xs),
        _ => Error
      }
  }

# Drop first n elements from list L
fun drop :: (Int, List[a]) -> List[a]
fun drop n l =
  case l of {
    Nil => l,
    Cons x xs =>
      case n of {
        0 => l,
        1 => xs,
        _ => drop (n - 1) xs
      }
  }

# (a -> Bool) -> [a] -> [a]
# Remove elements from beginning of [a] as long as predicate f is satisfied
fun dropWhile :: ((a) -> Int, List[a]) -> List[a]
fun dropWhile f l =
  case l of {
    Nil => l,
    Cons x xs =>
      let continue = f x in
      case continue of {
        0 => l,
        1 => dropWhile f xs,
        _ => Error
      }
  }

# Length of list L
fun length :: (List[a]) -> Int
fun length l = lengthrec 0 l

fun lengthrec :: (Int, List[a]) -> Int
fun lengthrec n l =
  case l of {
    Nil => n,
    Cons x xs => lengthrec (n + 1) xs
  }

# return a 2tuple of half-lists
fun split :: (List[a]) -> Tuple[List[a], List[a]]
fun split l =
  let {
    totallength = length l,
    halflength = srl totallength 1,
    list1 = take halflength l,
    list2 = drop halflength l
  } in Tuple list1 list2

fun foldr :: ((a, b) -> b, b, List[a]) -> b
fun foldr f z l =
  case l of {
    Nil => z,
    Cons x xs => f x (foldr f z xs)
  }

fun foldr1 :: ((a, a) -> a, List[a]) -> a
fun foldr1 f l =
  case l of {
    Cons x xs =>
      case xs of {
        Nil => x,
        Cons y ys => f x (foldr1 f xs)
      },
    Nil => Error
  }

fun foldl :: ((b, a) -> b, b, List[a]) -> b
fun foldl f z l =
  case l of {
    Nil => z,
    Cons x xs => foldl f (f z x) xs
  }

# zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
fun zipWith :: ((a, b) -> c, List[a], List[b]) -> List[c]
fun zipWith f arga argb =
  case arga of {
    Nil => Nil,
    Cons a as =>
      case argb of {
        Nil => Nil,
        Cons b bs => Cons (f a b) (zipWith f as bs)
      }
  }

# 0x12a
# nth :: Int -> [a] -> a
fun nth :: (Int, List[a]) -> a
fun nth n l =
  case l of {
    Cons h tl =>
      case n of {
        0 => h,
        _ => nth (n - 1) tl
      },
    Nil => Error
  }

# slice :: [a] -> Int -> Int -> [a]
# Gets the subsequence [from, until) in l
# This does no proper Error checking yet.
fun slice :: (List[a], Int, Int) -> List[a]
fun slice l from until = take (until - from) (drop from l)

# repeat :: a -> [a]
fun repeat :: (a) -> List[a]
fun repeat a = Cons a (repeat a)

# replicate :: Int -> a -> [a]
fun replicate :: (Int, a) -> List[a]
fun replicate n e = take n (repeat e)

# append :: [a] -> a -> [a]
# not very efficient, but it's quick for now
# TODO eventually replace with more efficient way
fun append :: (List[a], a) -> List[a]
fun append l e = concat l (Cons e Nil)

fun is_empty :: (List[a]) -> Int
fun is_empty l =
  case l of {
    Nil => 1,
    Cons x xs => 0
  }

# 0x130
# add elem to the list at position index
# it is an Error if index is out of bounds
# of the original list l, or if index is -1
fun updated :: (Int, a, List[a]) -> List[a]
fun updated index elem l =
  let len = length l in
  let oob = leq len index in
  case oob of {
    0 =>
      let before = take index l in
      let ip1 = add index 1 in
      let after = drop ip1 l in
      let l1 = append before elem in
      let l2 = concat l1 after in
      l2,
    _ => let e = Error in e
  }

# reverse the list l
fun reverse :: (List[a]) -> List[a]
fun reverse l = reverseRec l Nil

fun reverseRec :: (List[a], List[a]) -> List[a]
fun reverseRec from to =
  case from of {
    Nil => to,
    Cons x xs => reverseRec xs (Cons x to)
  }

# Print a list, where each element
# is not necessarily an int, by
# providing the printing function.
# See progs/updated-test.hm for an example usage.
fun print_list_p :: (List[a], (a -> Int)) -> Int
fun print_list_p l printer =
  case is_empty l of {
    1 => 0,
    0 =>
      let {
        hd = head l,
        tl = tail l,
        out = printer hd
      } in
      case out of { _ =>
        let space = putint 0 0x20 in
        case space of { _ =>
          print_list_p tl printer
        }
      },
    _ => Error
  }

# General quicksort function. Takes a list and a comparison function,
# which should return -1, 0, or 1 if a is less than, equal to, or greater
# than b.
fun qsort :: (List[a], ((a, a) -> Int)) -> List[a]
fun qsort inlist cmp =
  let {
    gt0 = leq 0,
    fgt = compose2 gt0 cmp,  # 1 iff cmp a b > 0
    fnot = xor 1,
    fleq = compose2 fnot fgt  # inverse; 1 iff cmp a b <= 0
  } in qsortrec inlist fleq fgt

fun qsortrec :: (List[a], (a, a) -> Int, (a, a) -> Int) -> List[a]
fun qsortrec inlist fleq fgt =
  case inlist of {
    Nil => inlist,
    Cons x xs =>
      let {
        smaller = filter xs (fleq x),
        larger = filter xs (fgt x),
        left = qsortrec smaller fleq fgt,
        right = qsortrec larger fleq fgt
      }
      in concat left (Cons x right)
  }

# Quicksort for integer list.
fun qsort_int :: (List[Int]) -> List[Int]
fun qsort_int inlist =
  let fleq = leq in
  let leqfunc = fleq in
  let invert = xor 1 in
  let fgt = compose2 invert leqfunc in
  qsortrec inlist fleq fgt

# Compare the strings (Lists of Ints) A B, returning -1, 0, or 1 if
# A is respectively less than, equal to, or greater than B.
fun strcmp :: (List[Int], List[Int]) -> Int
fun strcmp a b =
  case a of {
    Nil =>
      case b of {
        Nil => 0,  # two strings equal
	      Cons b bs => make32 0xFFFF 0xFFFF # A ended while B goes on; A < B
      },
    Cons a as =>
      case b of {
        Nil => 1,  # B ended while A goes on; A > B
      	Cons b2 bs2 =>
          let diff = a - b2 in
      	  case diff of {
      	    0 => strcmp as bs2, # a == b, compare rest of A and B
      	    _ => diff  # chars were different, sign encodes difference
      	  }
      }
  }

fun fst :: (Tuple[a, b]) -> a
fun fst t =
 case t of {
   Tuple x y => x
 }

fun snd :: (Tuple[a, b]) -> b
fun snd t =
 case t of {
   Tuple x y => y
 }

######################################################
# Binary Tree operations
######################################################

fun bt_isempty :: (BTNode[a]) -> Int
fun bt_isempty bt =
  case bt of {
    BTNodeNil => 1,
    BTNode x a b => 0
  }

fun bt_contains :: (BTNode[a], a, (a, a) -> Int, (a, a) -> Int) -> Int
fun bt_contains bt x feq flt =
  case bt of {
    BTNodeNil => 0,
    BTNode val left right =>
      case (feq x val) of {
        1 => 1,
        0 =>
  	      case (flt x val) of {
  	        1 => bt_contains left x feq flt,
  	        0 => bt_contains right x feq flt,
            _ => Error
  	      }
        _ => Error
      }
  }

fun bt_makeNode :: (a) -> BTNode[a]
fun bt_makeNode x =
  let empty = BTNodeNil in
  BTNode x empty empty

fun bt_insert :: (BTNode[a], a, (a,a) -> Int, (a,a) -> Int) -> BTNode[a]
fun bt_insert bt x feq flt =
  case bt of {
    BTNodeNil => bt_makeNode x,
    BTNode val left right =>
      case (feq x val) of {
        1 => BTNode x left right,
        0 =>
          case (flt x val) of {
            1 =>
              let newLeft = bt_insert left x feq flt in
              BTNode val newLeft right,
            0 =>
              let newRight = bt_insert right x feq flt in
              BTNode val left newRight,
            _ => Error
          }
        _ => Error
      }
  }

fun bt_delete :: (BTNode[a], a, (a,a) -> Int, (a,a) -> Int) -> BTNode[a]
fun bt_delete bt x feq flt =
  case bt of {
    BTNodeNil => bt,
    BTNode val left right =>
      case (feq x val) of {
        1 => bt_deleteNode bt feq flt,
        0 =>
          case (flt x val) of {
            1 =>
              let newLeft = bt_delete left x feq flt in
              BTNode val newLeft right,
            0 =>
              let newRight = bt_delete right x feq flt in
              BTNode val left newRight,
            _ => Error
          }
        _ => Error
      }
  }

# Delete this node
fun bt_deleteNode :: (BTNode[a], (a,a) -> Int, (a,a) -> Int) -> BTNode[a]
fun bt_deleteNode bt feq flt =
  case bt of {
    BTNodeNil => bt,
    BTNode val left right =>
      case left of {
        BTNodeNil => right,
        BTNode x y z =>
          case right of {
  	        BTNodeNil => left,
  	        BTNode a b c =>
  	          let newVal = bt_min right in
  	          let newRight = bt_delete right newVal feq flt in
  	          BTNode newVal left newRight
  	      }
      }
  }

# 0x13a
fun bt_min :: (BTNode[a]) -> a
fun bt_min bt =
  case bt of {
    BTNode val left right =>
      case left of {
        BTNodeNil => val,
        BTNode a b c => bt_min left
      },
    BTNodeNil => Error # TODO check with Joseph (return a default 'a' value?)
  }

######################################################
# Binary Tree Array
######################################################

# Make an array of size N with supplied zero
fun bta_make :: (Int, a) -> BTArray[a]
fun bta_make n btazero = bta_makeArrayRec 0 n btazero

fun bta_makeArrayRec :: (Int, Int, a) -> BTArray[a]
fun bta_makeArrayRec from to btazero =
  let end = BTArrayNil in
  case (to - from) of {
    0 =>  # no values, return empty node
      end,
    1 =>  # 1 value; put in node with two empty children
      BTArray from btazero end end,
    _ =>  # Otherwise, put middle value in this node and recur
      let {
        middle = srl (from + to) 1,
        left = bta_makeArrayRec from middle btazero,
        right = bta_makeArrayRec (middle + 1) to btazero
      } in
      BTArray middle btazero left right
  }

fun bta_get :: (BTArray[a], Int) -> a
fun bta_get array index  =
  case array of {
    BTArray i v left right =>
      case (eq i index) of {
        0 =>
  	      case (lt index i) of {
  	        1 => bta_get left index,
  	        0 => bta_get right index,
            _ => Error
  	      },
        1 => v,
        _ => Error
      },
    BTArrayNil => Error # TODO check with Joseph (return a default 'a' value?)
  }

fun bta_put :: (BTArray[a], Int, a) -> BTArray[a]
fun bta_put array index value =
  case array of {
    BTArray i v left right =>
      case (eq i index) of {
        1 => BTArray i value left right,
        0 =>
          case (lt index i) of {
        	  1 =>
        	    let newLeft = bta_put left index value in
        	    BTArray i v newLeft right,
        	  0 =>
        	    let newRight = bta_put right index value in
        	    BTArray i v left newRight,
            _ => Error
        	},
        _ => Error
      },
    BTArrayNil => BTArrayNil
  }

fun bta_print :: (BTArray[Int], Int) -> Int
fun bta_print array size = bta_printRec array 0 size

# 0x140
fun bta_printRec :: (BTArray[Int], Int, Int) -> Int
fun bta_printRec array i end =
  case (eq i end) of {
    1 => 0,
    0 =>
      let out = bta_printnode array i in
      let rec = bta_printRec array (i + 1) end in
      case out of { _ => rec }
   _ => Error
  }

fun bta_printnode :: (BTArray[Int], Int) -> Int
fun bta_printnode array index =
  let out = putint 0 (bta_get array index) in
  case out of { _ => 0 }

######################################################
# Queue
######################################################

fun qMake :: Queue[a]
fun qMake =
  let n = Nil in
  Queue n n

# Return the element at the front of the queue
fun qGet :: (Queue[a]) -> a
fun qGet q =
  case q of {
    Queue f r =>
      case f of {
        Cons x xs => x,
        Nil => head (reverse r)
      }
  }

# Drop the first element of the queue
fun qDrop :: (Queue[a]) -> Queue[a]
fun qDrop q =
  case q of {
    Queue f r =>
      case f of {
        Cons x xs =>
          case xs of {
            Cons y ys => Queue xs r,
            Nil => Queue (reverse r) Nil
          },
        Nil => qDrop (Queue (reverse r) Nil)
      }
  }

# Put an item at the end of the queue
fun qPut :: (Queue[a], a) -> Queue[a]
fun qPut q x =
  case q of {
    Queue f r => Queue f (Cons x r)
  }

############################################################
# IntMap
###########################################################
# Implementation lifted from
# https://hackage.haskell.org/package/containers-0.5.11.0/docs/src/Data.IntMap.Internal.html

fun intmap_make :: IntMap[a]
fun intmap_make = IntMapEmpty

fun intmap_insert :: (Int, a, IntMap[a]) -> IntMap[a]
fun intmap_insert k x t =
  case t of {
    IntMap p m l r =>
      case (nomatch k p m) of {
        1 => link k (IntMapLeaf k x) p t,
        0 =>
          case (zero k m) of {
            1 =>
              let left = intmap_insert k x l in
              IntMap p m left r,
            0 =>
              let right = intmap_insert k x r in
              IntMap p m l right,
            _ => Error
          },
        _ => Error
      },
    IntMapLeaf ky vy =>
      case (eq k ky) of {
        1 => IntMapLeaf k x,
        0 => link k (IntMapLeaf k x) ky t,
        _ => Error
      },
    IntMapEmpty => IntMapLeaf k x
  }

fun intmap_delete :: (Int, IntMap[a]) -> IntMap[a]
fun intmap_delete k t =
  case t of {
    IntMap p m l r =>
      case (nomatch k p m) of {
        1 => t,
        0 =>
          case (zero k m) of {
            1 =>
              let ldel = intmap_delete k l in
              binCheckLeft p m ldel r,
            0 =>
              let rdel = intmap_delete k r in
              binCheckRight p m l rdel,
            _ => Error
          }
        _ => Error
      },
    IntMapLeaf ky vy =>
      case (eq k ky) of {
        1 => IntMapEmpty,
        0 => t,
        _ => Error
      },
    IntMapEmpty => IntMapEmpty
  }

fun intmap_lookup :: (Int, IntMap[a]) -> Maybe[a]
fun intmap_lookup k t =
  case t of {
    IntMap p m l r =>
      case (nomatch k p m) of {
        1 => Nothing,
        0 =>
          case (zero k m) of {
            1 => intmap_lookup k l,
            0 => intmap_lookup k r,
            _ => Error
          },
        _ => Error
      },
    IntMapLeaf kx x =>
      case (eq k kx) of {
        1 => Just x,
        0 => Nothing,
        _ => Error
      },
    IntMapEmpty => Nothing
  }

# Check that the left subtree is not empty
fun binCheckLeft :: (Int, Int, IntMap[a], IntMap[a]) -> IntMap[a]
fun binCheckLeft p m l r =
  case l of {
    IntMapEmpty => r,
    _ => IntMap p m l r
  }

# Check that the right subtree is not empty
fun binCheckRight :: (Int, Int, IntMap[a], IntMap[a]) -> IntMap[a]
fun binCheckRight p m l r =
  case r of {
    IntMapEmpty => l,
    _ => IntMap p m l r
  }

# Does the key i differ from the prefix p before getting to the switching bit m?
fun nomatch :: (Int, Int, Int) -> Int
fun nomatch i p m =
  let {
    flipped = neg (m - 1),
    mask = flipped ^ m,
    masked = i & mask,
    equal = eq masked p
  }
  in 1 ^ equal

fun link :: (Int, IntMap[a], Int, IntMap[a]) -> IntMap[a]
fun link p1 t1 p2 t2 =
  let m = branchMask p1 p2 in
  let maskflip = neg (m - 1) in
  let mask = maskflip ^ m in
  let p = p1 & mask in
  case (zero p1 m) of {
    1 => IntMap p m t1 t2,
    0 => IntMap p m t2 t1,
    _ => Error
  }

# The first switchign bit where the two prefixes disagree
fun branchMask :: (Int, Int) -> Int
fun branchMask p1 p2 = highestBitMask (p1 ^ p2)

# Return an Int where only the highest bit is set
fun highestBitMask :: (Int) -> Int
fun highestBitMask x1 =
  let {
    x1s = srl x1 1,
    x2 = x1 | x1s,
    x2s = srl x2 2,
    x3 = x2 | x2s,
    x3s = srl x3 4,
    x4 = x3 | x3s,
    x4s = srl x4 8,
    x5 = x4 | x4s,
    x5s = srl x5 16,
    x6 = x5 | x5s,
    x6s = srl x6 1
  }
  in x6 ^ x6s

# Should this key follow the left subtree of a tree with switching bit m?
# Answer only valid when match i p m is true.
fun zero :: (Int, Int) -> Int
fun zero i m = eq (i & m) 0

##############################################################
# List comprehension and helpers
# (greater than 3 can be created pretty much programmatically)
##############################################################
# comp2
# comp3
# cartesianProduct

fun comp2 :: ((a, b) -> c, List[a], List[b]) -> List[c]
fun comp2 f l1 l2 =
  let m = map in
  concatAll (m (flip m l2) (m f l1))

fun comp3 :: ((a, b, c) -> d, List[a], List[b], List[c]) -> List[d]
fun comp3 f l1 l2 l3 =
  let m = map in
  let t3 = concatAll (m (flip m l2) (m f l1)) in
  concatAll (map (flip m l3) t3)

fun cartesianProduct :: (List[List[Int]]) -> List[List[Int]]
fun cartesianProduct lists =
 case lists of {
   Nil => Nil,
   Cons h tl =>
     case tl of {
       Nil => map (append Nil) h,
       _ =>
         let f = prependHelper (cartesianProduct tl) in
         concatAll (map f h)
     }
  }

fun prependHelper :: (List[List[Int]], Int) -> List[List[Int]]
fun prependHelper l x = map (Cons x) l

######################################################
# List creators/helpers
######################################################
# fromThen
# fromTo
# fromThenTo
# infFrom
# infFromThen

# [2,4,..]
# or
# [10,8..]
fun fromThen :: (Int, Int) -> List[Int]
fun fromThen first next =
  case (lt first next) of {
    0 => fromThenTo first next 0,
    _ => let rest = fromThen next ((next - first) + next) in
         Cons first rest
  }

# [1..10]
# or
# [10..1]
fun fromTo :: (Int, Int) -> List[Int]
fun fromTo first last =
  case (lt first last) of {
    0 => fromThenToDown first (first - 1) last,
    _ => fromThenToUp first (first + 1) last
  }

# helper for fromThenTo
fun fromThenToDown :: (Int, Int, Int) -> List[Int]
fun fromThenToDown first next last =
  case (lt first 0) of {
    0 => case (lt first last) of {
           0 => let rest = fromThenToDown next (next + (next - first)) last in
                Cons first rest,
           _ => Nil
         },
    _ => Nil
  }

# helper for fromThenTo
fun fromThenToUp :: (Int, Int, Int) -> List[Int]
fun fromThenToUp first next last =
  case (lt next first) of {
    0 => case (lt last first) of {
           0 => let rest = fromThenToUp next (next + (next - first)) last in
                Cons first rest,
           _ => Nil
         },
    _ => Nil
  }

# [1,3..10]
# or
# [9,7..1]
fun fromThenTo :: (Int, Int, Int) -> List[Int]
fun fromThenTo first next last =
  case (lt first last) of {
    0 => fromThenToDown first next last,
    _ => fromThenToUp first next last
  }

# I'm not sure how differs from fromThen
fun infFromThen :: (Int, Int) -> List[Int]
fun infFromThen first next = fromThen first next

fun id :: (a) -> a
fun id x = x

fun apply :: ((a) -> b, a) -> b
fun apply f x = f x
{-# LANGUAGE FlexibleContexts #-}

module Main where

import Control.Monad (when)
import Control.Monad.Except (runExceptT, ExceptT, liftEither, withExceptT)
import Control.Monad.State (runStateT, StateT)
import Data.Text.Prettyprint.Doc (pretty)
import Data.Either (partitionEithers)
import System.Exit
import Text.Megaparsec (runParserT, parseErrorPretty')
import Data.List (isSuffixOf)
import qualified Data.List.NonEmpty as NEL
import qualified Options.Applicative as OptAp

import qualified Zarf.Parser as P
import qualified Zarf.ThmParser as PThms
import Zarf.Analyze
import Zarf.Common (Context, ErrorInfo(..), builtins, ZarfError(OtherError), Name(..))
import Zarf.Translate (translateProg)
import qualified Zarf.IR as IR
import qualified Zarf.Ast as Ast
import qualified Zarf.Cli as Cli
import qualified Zarf.Typing as Typing

main :: IO ()
main = do
    options <- OptAp.execParser Cli.cmdParser
    let (includeFiles, srcFiles) = NEL.partition (isSuffixOf ".thm") $ Cli.inputFiles options
    when (length srcFiles == 0) (putStrLn "Warning: no source files")
    includeProgs <- collectParseResults (const PThms.program) includeFiles
    srcProgs <- collectParseResults P.program srcFiles
    case (fst includeProgs, fst srcProgs) of
      ([], []) ->
        runPipeline (Cli.outputFile options) (Cli.squash options) (snd includeProgs) (snd srcProgs)
      (incErrors, srcErrors) -> do
        reportErrors incErrors "Errors while handling include files:"
        reportErrors srcErrors "Errors:"
        pure ()
  where
    -- | Parses every file, returning a list of failures and successes
    collectParseResults parser fnames =
      -- (Read this starting from the last line)
      -- Split into errors and successes
      partitionEithers .
      -- Group each error and each success with the source contents
      fmap distributeProd <$>
        -- Parse every single file, short-circuiting on error
        sequence (parseSourceFile parser <$> fnames)
    -- | Parses every source file, returning a pair of source contents and AST
    parseSourceFile parser fname = do
      src <- readFile fname
      ((,) src) <$> runParserT (parser fname) fname src
    distributeProd :: (a, Either b c) -> Either (a, b) (a, c)
    distributeProd (x, Left y) = Left (x, y)
    distributeProd (x, Right z) = Right (x, z)

    reportErrors errList msg = do
      when (length errList > 0 && length msg > 0) (putStrLn msg)
      mapM_ (\(src, e) -> putStrLn $ parseErrorPretty' src e) errList

-- | Executes translation pipeline, with an optional output file, a squash
-- setting, the include files, and the source files. The pairs in the include
-- and source file arguments consist of the source input text and the
-- corresponding parsed ASTs.
runPipeline :: Maybe String ->
               Bool ->
               [(String, Ast.Program)] ->
               [(String, IR.ProgramF IR.SourcedExpr)] ->
               IO ()
runPipeline outputFile squash includes srcs = do
  (astRes, _) <- runStateT (runExceptT (
                               pipeline (snd <$> includes) (snd <$> srcs))) builtins
  case astRes of
    Right ast ->
      let output = show (pretty ast) in
      case outputFile of
        Just dest -> writeFile dest output
        Nothing -> do
          if squash
            then putStrLn (unlines (fst <$> includes))
            else pure ()
          putStrLn output
    Left pipelineErrs -> do
      putStrLn "Error: "
      mapM_ (putStrLn . show . pretty) pipelineErrs
      exitFailure


pipeline :: (Monad m) =>
            [Ast.Program] ->
            [IR.ProgramF IR.SourcedExpr] ->
            ExceptT [ErrorInfo] (StateT Context m) Ast.Program
pipeline includes irs = do
  mapM_ putIncludeBindings includes
   -- TODO: temporary while annotated ASTs are being integrated
  let unannIRs = fmap IR.unannotate <$> irs
  let prog = head irs
  let typingEnv = Typing.collectTopLevelBinds prog Typing.defaultEnv
  withExceptT (pure . MkError (MkName "foo") . OtherError . show) . liftEither $
    Typing.runTypecheck typingEnv (Typing.typecheckProgram prog)
  mapM_ putBindings unannIRs
  mconcat <$> mapM translateProg unannIRs

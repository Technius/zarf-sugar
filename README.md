# zarf-sugar

This repository contains both the high-level Zarf transpiler and the
experimental Zarf backend for GHC.

## Compiling and Installing

Use `stack build` to compile, and `stack install` to install both programs
(`zarf-hl` and `zarf-ghc`).

To test the Zarf backend for GHC. run `stack exec zarf-ghc -- <file name>`. For
example, try `stack exec zarf-ghc -- <file name>`

## Usage

* Run `zarf-hl <files> -o <output_file>` to generate code.
* `zarf-hl` accepts `*.thms` files as input, but it can also take `*.thm` files
to populate its symbol table.
* To output the contents of the `*.thm` files with the translated `*.thms`
files, use the `--squash` option.

Note: `zarf-hl` has a type checker, but it is very much buggy and accepts
programs that may not be well-typed. Make sure to always verify that it works
with the actual simulator.

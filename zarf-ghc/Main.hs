module Main where

import Control.Monad (when)
import Control.Monad.IO.Class

-- Library imports
import Data.Text.Prettyprint.Doc (pretty)
import Control.Monad.State.Strict (runState)
import qualified Options.Applicative as OptAp
import qualified Data.List.NonEmpty as NonEmpty

-- GHC imports
import CorePrep (corePrepPgm)
import SimplCore (core2core)
import TidyPgm (tidyProgram)
import CoreSyn
import DynFlags
import GHC
import GHC.Paths (libdir)
import HscTypes (CgGuts(..))
import CoreZarf.CoreTrans
import Outputable

import qualified CoreZarf.Cli as Cli

import Zarf.Translate (translateProg)
import Zarf.Common (builtins)
import Zarf.Ast ()

main :: IO ()
main = do
  options <- OptAp.execParser Cli.cmdParser
  let fileName = NonEmpty.head $ Cli.inputFiles options
  when (Cli.debug options) $ putStrLn $ "Reading from " ++ fileName
  (dynFlags, src, adts) <- defaultErrorHandler defaultFatalMessager defaultFlushOut $
    runGhc (Just libdir) (pipeline fileName)
  if not (Cli.debug options)
    then generateIR dynFlags src adts
    else displayAst dynFlags src adts

displayAst :: DynFlags -> [CoreBind] -> [TyCon] -> IO ()
displayAst dynFlags src adts = do
  putStrLn (showSDoc dynFlags (ppr (stripIrrelevant src)))
  putStrLn "ADTs:"
  mapM_ putStrLn (show . tyConToZarf dynFlags <$> adts)
  putStrLn "Functions:"
  mapM_ (putStrLn . bindToZarf' dynFlags) (stripIrrelevant src)
  putStrLn "Translated attempt:"
  mapM_ (print . bindToZarf dynFlags) (stripIrrelevant src)

generateIR :: DynFlags -> [CoreBind] -> [TyCon] -> IO ()
generateIR dflags src adts = do
  let prog = coreToZarf dflags "HaskellSrc" adts src
  let (result, _) = runState (translateProg prog) builtins
  print (pretty result)

pipeline :: String -> Ghc (DynFlags, CoreProgram, [TyCon])
pipeline fileName = do
  dynFlags <- getSessionDynFlags
  let dynFlags' = dynFlags { hscTarget = HscNothing
    }
  setSessionDynFlags dynFlags'
  env <- getSession
  target <- guessTarget fileName Nothing
  setTargets [target]
  load LoadAllTargets
  modSum <- getModSummary $ mkModuleName (takeWhile (/= '.') fileName)
  p <- parseModule modSum
  t <- typecheckModule p
  d <- desugarModule t
  let core = coreModule d
  simpl <- liftIO $ core2core env core
  (tidied, _) <- liftIO $ tidyProgram env simpl
  (prepped, _) <- liftIO $
    corePrepPgm env (cg_module tidied) (ms_location modSum) (cg_binds tidied) (cg_tycons tidied)
  pure (dynFlags', prepped, cg_tycons tidied)

showCore :: Outputable s => DynFlags -> s -> String
showCore dflags x = showSDoc dflags (ppr x)

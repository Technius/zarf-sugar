module CoreZarf.Cli where

import Options.Applicative
import Data.List.NonEmpty
import Data.Semigroup ((<>))

data Command = CmdCompile { inputFiles :: NonEmpty String
                          , outputFile :: Maybe String
                          , debug :: Bool }

cmdParser :: ParserInfo Command
cmdParser = info compileCmd (fullDesc
                             <> progDesc "Codegen for Zarf via GHC")

compileCmd :: Parser Command
compileCmd = CmdCompile <$> inputFile
           <*> (optional $ strOption (long "out" <> short 'o' <> metavar "out" <> help "Output file"))
           <*> (switch (long "debug" <> short 'd' <> help "Show debug information"))
  where inputFile = some1 $ argument str
                    (metavar "<file>"
                     <> help "Input source files")

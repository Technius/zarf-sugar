module CoreZarf.CoreTrans where

-- Library imports

import Data.List (intercalate)
import Data.List.NonEmpty (NonEmpty(..))

import Data.Maybe (fromMaybe)
import Control.Applicative ((<|>))

-- GHC imports
import DynFlags
import Outputable
import qualified CoreSyn as C
import Var
import TyCoRep
import TyCon
import qualified Type as HsType
import DataCon
import CoreUtils (exprType)
import qualified PrelNames
import qualified Name as HsName
import qualified Literal as HsLit

-- Zarf imports
import qualified Zarf.IR as Z
import Zarf.Common

coreToZarf :: DynFlags -> String -> [TyCon] -> [C.CoreBind] -> Z.Program
coreToZarf dflags name adts fns =
  let binds' = bindToZarf dflags <$> stripIrrelevant fns
      adts' = tyConToZarf dflags <$> adts
  in Z.Prog name (adts' ++ binds')

-- | Translates a data declaration.
tyConToZarf :: DynFlags -> TyCon -> Z.Decl
tyConToZarf dflags con =
  let name = showCore dflags (tyConName con)
      ctors = dataConToZarf dflags <$> tyConDataCons con
      tvars = MkVar . showCore dflags <$> tyConTyVars con
  in Z.DataTy (MkName name) tvars ctors

-- | Translates a constructor in a data declaration.
dataConToZarf :: DynFlags -> DataCon -> Z.Constructor
dataConToZarf dflags dcon =
  let name = showCore dflags (dataConName dcon)
      args = tyToZarf dflags <$> dataConOrigArgTys dcon
  in Z.Ctor (MkName name) args

-- | Translates a Haskell type into a Zarf type.
tyToZarf :: DynFlags -> Type -> Z.Type
tyToZarf dflags t = translate $ snd (HsType.splitForAllTys t)
  where
    translate ty = fromMaybe (cons ty) (tvar ty <|> fun ty)
    tvar ty = Z.TyVar . MkVar . showCore dflags <$> HsType.getTyVar_maybe ty
    cons ty =
      let name = showCore dflags $ tyConName (HsType.tyConAppTyCon ty)
          tvars = tyToZarf dflags <$> HsType.tyConAppArgs ty
      in if name == "Int"
         then Z.intType
         else Z.TyData (MkName name) tvars
    fun ty =
      let (args, res) = HsType.splitFunTys ty in
        if null args
          then Nothing
          else Just $ Z.tyMkFun (tyToZarf dflags <$> args) (tyToZarf dflags res)

-- | Strips functions not required for translation
stripIrrelevant :: [C.CoreBind] -> [C.CoreBind]
stripIrrelevant = filter f
  where f :: C.CoreBind -> Bool
        f (C.NonRec bndr expr) = testType bndr (exprType expr)
        f (C.Rec _) = False
        testType :: C.CoreBndr -> Type -> Bool
        testType bndr ty
          | HsType.isPrimitiveType ty = False
          | HsType.isAlgType ty =
            let (tc, args) = HsType.splitTyConApp ty
                name = tyConName tc
                -- There are bindings with type [KindRep]
                -- so we need to filter then out too
            in if PrelNames.getUnique name == PrelNames.listTyConKey
                  then testType bndr (head args)
                  else testTyconName (tyConName tc) && testNotConFun bndr tc
          | otherwise = True
        -- | Filter out runtime type information
        testTyconName n = (n `notElem`)
          [ PrelNames.kindRepTyConName
          , PrelNames.trNameTyConName
          , PrelNames.trTyConTyConName
          , PrelNames.trModuleTyConName
          ]
        -- | Filter out constructor functions
        testNotConFun bndr tcs =
          let dcs = tyConDataCons tcs
              workers = dataConWorkId <$> dcs -- TODO: Why doens't this filter everything?
              wrappers = dataConWrapId <$> dcs
          in all ((/=) bndr) (workers ++ wrappers)

placeholderExpr:: Z.Expr
placeholderExpr = (Z.Var . MkVar $ "cannot translate")

placeholder :: DynFlags -> HsName.Name -> Z.Decl
placeholder dflags name =
  Z.Func (MkName (showCore dflags name)) [] Z.intType [] placeholderExpr

-- | Translates a top-level Haskell function to Zarf
funToZarf :: DynFlags -> HsName.Name -> C.CoreExpr -> Z.Decl
funToZarf dflags name expr =
  let fname = MkName (showCore dflags name)
      ty = tyToZarf dflags (exprType expr)
      (hsTyVars, hsVars, expr') = C.collectTyAndValBinders expr
      tyVars = MkVar . showCore dflags <$> hsTyVars
      argNames = MkVar . showCore dflags . varName <$> hsVars
      zExpr = exprToZarf dflags expr'
  in Z.Func fname tyVars ty argNames zExpr


-- | Translates a Haskell expression (that is not a top-level binding) to Zarf
exprToZarf :: DynFlags -> C.CoreExpr -> Z.Expr
exprToZarf dflags (C.Let (C.NonRec bnd val) expr) =
  Z.Let (MkVar (showCore dflags bnd)) (exprToZarf dflags val) (exprToZarf dflags expr)
exprToZarf dflags expr@(C.App _ _) =
  let (fun, args0) = C.collectArgs expr
      args = filter (not . C.isTypeArg) args0
  in case appPrim dflags fun args0 of
       Just expr' -> expr'
       Nothing ->
         case args of
           [] -> Z.App (exprToZarf dflags fun) (exprToZarf dflags expr :| [])
           (ha : as) ->
             let args' =(exprToZarf dflags ha :| (exprToZarf dflags <$> as))
             in Z.App (exprToZarf dflags fun) args'
         -- Z.Var (MkVar ("apply " ++ zFun ++ " to " ++ zArgs))
exprToZarf dflags (C.Var v) = Z.Var (MkVar (showCore dflags (varName v)))
exprToZarf _ (C.Lit _) = Z.Var (MkVar "Some literal") -- TODO: fill in
exprToZarf _ _ = placeholderExpr

-- | Translates primitive function applications
appPrim :: DynFlags -> C.CoreExpr -> [C.CoreExpr] -> Maybe Z.Expr
-- Relevant modules: PrelNames, TysPrim, TysWiredIn
appPrim dflags (C.Var funId) [C.Lit (HsLit.MachInt i)] -- Int literals
  -- Really bad hack, since I can't find the Unique for I# (Int constructor)
  -- It doesn't match PrelNames.intDataConKey
  | showCore dflags funId == "I#" =
      Just $ Z.ConstInt (fromIntegral i)
  | otherwise = Nothing
-- Binary ops on ints
appPrim dflags (C.Var funId) [C.Type ty, C.Var _, C.Var arg1, C.Var arg2] =
  let f opName hsName = primBinOp dflags opName hsName funId ty arg1 arg2
  in f "plus" "+" <|> f "sub" "-" <|> f "mult" "*" <|> f "div" "div"
appPrim _ _ _ = Nothing

primBinOp :: DynFlags -> String -> String -> Id -> Type -> Id -> Id -> Maybe Z.Expr
primBinOp dflags opName hsName funId ty arg1 arg2 =
  if showCore dflags funId == hsName
    then case HsType.tyConAppTyCon_maybe ty of
           Just tc ->
             if tyConUnique tc == PrelNames.intTyConKey
               then let a1 = Z.Var . MkVar $ showCore dflags arg1
                        a2 = Z.Var . MkVar $ showCore dflags arg2
                    in Just $ Z.App (Z.Var $ MkVar opName) (a1 :| [a2])
               else Nothing
           Nothing -> Nothing
    else Nothing


bindToZarf :: DynFlags -> C.CoreBind -> Z.Decl
bindToZarf dflags (C.NonRec name expr) = funToZarf dflags (varName name) expr
bindToZarf dflags (C.Rec pairs) = placeholder dflags (varName (fst (head pairs)))

-- Constructors have type TyCon
-- need a pass to remove TyCon binds and generate ADTs
-- Can probably ignore `TyName`s; they don't seem to do much
-- TyNames and KindReps are for runtime representation
bindToZarf' :: DynFlags -> C.CoreBind -> String
bindToZarf' dflags (C.NonRec name _) =
  "Bind(" ++ showCore dflags name ++ ")" ++
  " type=" ++ showCore dflags (varType name)
bindToZarf' dflags (C.Rec pairs) = intercalate "," $
  fmap
  (\(name, _) -> "RecBind(" ++ showCore dflags name ++ ")" ++
  " type=" ++ showCore dflags (varType name)) pairs

showCore :: Outputable s => DynFlags -> s -> String
showCore dflags x = showSDoc dflags (ppr x)

getDefinedType :: DynFlags -> C.CoreBind -> Maybe String
getDefinedType dflags (C.NonRec name _) = case varType name of
  AppTy t1 t2 -> Just $ "Apply " ++ showCore dflags t1 ++ " to " ++ showCore dflags t2
  TyConApp con _ ->
    if isAlgTyCon con then Just $ showCore dflags name ++ ": " ++ showCore dflags (tyConDataCons con)
                      else Nothing
  _ -> Nothing
getDefinedType _ _ = error "Unexpected recursive bind"

printAdt :: DynFlags -> TyCon -> Maybe String
printAdt dflags adt
  | isTyConWithSrcDataCons adt = Just (showCore dflags (tyConDataCons adt))
  | otherwise = Nothing

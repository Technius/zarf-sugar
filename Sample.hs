{-#LANGUAGE BangPatterns #-}

module Sample where

foo :: Int
foo = 1

foo2 :: Int
foo2 = 2

plus_ :: Int -> Int -> Int
plus_ a b = a + b

minus_ :: Int -> Int -> Int
minus_ a b = a - b

mult_ :: Int -> Int -> Int
mult_ a b = a * b

div_ :: Int -> Int -> Int
div_ a b = a `div` b

averageInt :: Int -> Int -> Int
averageInt a b = (a + b) `div` 2

bar :: MyDat
bar = Foo foo

myConst :: MyDat -> MyDat
myConst _ = bar

-- bar :: String
-- bar = "asdf"
-- 
-- fibonacci :: Int -> Int
-- fibonacci n = go 1 1 n
--   where go a _ 0 = a
--         go a b m = go b (a + b) (m - 1)

data MyBool = T | F
data MyDat = Foo Int | Bar MyBool MyBool

data MyMaybe a = MJust a | MNothing
data MyFun = FFun (Int -> String)

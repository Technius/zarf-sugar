module ZarfPrelude where

data Bool = True | False

data Maybe a = Just a | Nothing

data List a = Nil | Cons a (List a)

id :: a -> a
id x = x

const :: a -> b -> a
const a b = a

{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Zarf.Common where

import Control.Monad.Except
import Control.Monad.State.Class
import Data.Map (Map)
import Data.Text.Prettyprint.Doc (Pretty, (<>))
import qualified Data.Text.Prettyprint.Doc as Print
import qualified Data.Map as Map

newtype Variable = MkVar { getVarName :: String } deriving (Eq, Ord)
newtype Name = MkName { getName :: String } deriving (Eq, Ord)

-- | Describes the type of a name binding, along with the file in which it is
-- bound.
data BindingTy = BCtor String
               | BVar String
               deriving (Eq, Show)

getBindingFile :: BindingTy -> String
getBindingFile (BCtor f) = f
getBindingFile (BVar f) = f

data ErrorInfo = MkError Name ZarfError

-- | Main error data type
data ZarfError
  -- | A binding with this name was already found while attempting to insert
  -- another binding with the same name
  = RepeatedBinding Name String
  -- | Any other error
  | OtherError String

type Context = Map String BindingTy

type ContextM m = (MonadState Context m)

builtins :: Context
builtins = Map.fromList
  [ ("Error", BCtor "<prelude>")
  , ("putint", BCtor "<prelude>")
  , ("getint", BCtor "<prelude>") ]

type ErrorM m = (MonadError [ErrorInfo] m, MonadPlus m)

instance Show Variable where
  show v = show (getVarName v)

instance Show Name where
  show n = show (getName n)

instance Pretty Name where
  pretty v = Print.pretty (getName v)

instance Pretty Variable where
  pretty v = Print.pretty (getVarName v)

instance Pretty ZarfError where
  pretty (RepeatedBinding name defname) =
    Print.hsep [Print.pretty name, "already defined in file", Print.pretty defname]
  pretty (OtherError msg) =
    Print.pretty msg

instance Pretty ErrorInfo where
  pretty (MkError fname err) =
    Print.nest 2 $
      Print.hsep ["In file", Print.pretty fname] <> ":" <> Print.line
      <> Print.pretty err

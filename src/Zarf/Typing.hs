{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Zarf.Typing where

import Control.Unification
import Control.Unification.IntVar
  ( IntVar, IntBindingState, IntBindingT, evalIntBindingT )
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.List.NonEmpty as NE
-- import qualified Data.Maybe as Maybe
import Data.List (find, foldl')

import Data.Functor.Identity
import Data.Functor.Foldable (cata)

import Control.Monad (when)
import Control.Monad.Reader
import Control.Monad.Except (ExceptT, MonadError, throwError, runExceptT)

import Zarf.IR
import Zarf.Common

-- * Typing environment

-- | Lifts a [Type] into a [UType]
typeToUtype :: Type -> UType
typeToUtype = cata UTerm

-- | Attempts to convert a [UType] into a [Type], failing if there is a free
-- variable.
utypeToType :: UType -> Maybe Type
utypeToType (UTerm t) = case t of
  TyVarF x -> Just (TyVar x)
  TyDataF n args -> TyData n <$> traverse utypeToType args
  TyFunF arg res -> do
    arg' <- utypeToType arg
    res' <- utypeToType res
    pure $ TyFun arg' res'
utypeToType (UVar _) = Nothing

-- | [UType] version of [tySplitFun]
utypeSplitFun :: UType -> ([UType], UType)
utypeSplitFun (UTerm (TyFunF arg res)) =
  let (args, res') = utypeSplitFun res in (arg : args, res')
utypeSplitFun t = ([], t)

utypeMkFun :: [UType] -> UType -> UType
utypeMkFun [] res = res
utypeMkFun (a:as) res = UTerm (TyFunF a (utypeMkFun as res))

data IREnv = MkIREnv
  { envVars :: Map String UType
  , envTyVars :: Set String
  , envDataTys :: Map String (Type, [Constructor])
  }

-- | Looks up the data type, returning its type and its constructors.
envLookupDataTy :: String -> IREnv -> Maybe (Type, [Constructor])
envLookupDataTy name = Map.lookup name . envDataTys

-- | Looks up the data constructor, returning the associated type and the
-- constructor.
envLookupDataCon :: String -> IREnv -> Maybe (Type, Constructor)
envLookupDataCon name = msum . fmap (searchTy name) . Map.toList . envDataTys
  where
    searchTy n (_, (ty, dcons)) = (ty, ) <$> searchDcons n dcons
    searchDcons n = find (\case Ctor dcn _ -> n == getName dcn)

-- | Inserts the given algebraic data type
envInsertTyData :: String -> Type -> [Constructor] -> IREnv -> IREnv
envInsertTyData name ty ctors env =
  env { envDataTys = Map.insert name (ty, ctors) (envDataTys env)
      , envVars = Map.union (envVars env) $ Map.fromList $
          (\case ctor@(Ctor n _) ->
                   let cty = typeToUtype (getCtorType ctor ty)
                   in (getName n, cty)) <$> ctors
      }

-- | Returns all free type variables
envFreeTyVars :: UType -> IREnv -> Set String
envFreeTyVars t env = case t of
  UTerm (TyDataF _ args) -> Set.unions (flip envFreeTyVars env <$> args)
  UTerm (TyVarF v) ->
    if envHasTyVar (getVarName v) env
      then Set.empty
      else Set.singleton (getVarName v)
  UTerm (TyFunF arg res) -> Set.unions $
    envFreeTyVars res env : [envFreeTyVars arg env]
  UVar _ -> Set.empty

-- | Updates type variables in the typing environment
envUpdateTyVars :: (Set String -> Set String) -> IREnv -> IREnv
envUpdateTyVars f env = env { envTyVars = f (envTyVars env) }

updateEnvVars :: (Map String UType -> Map String UType) -> IREnv -> IREnv
updateEnvVars f env = env { envVars = f (envVars env) }

envLookupVar :: String -> IREnv -> Maybe UType
envLookupVar name = Map.lookup name . envVars

envHasTyVar :: String -> IREnv -> Bool
envHasTyVar name = Set.member name . envTyVars

defaultEnv :: IREnv
defaultEnv =
  let {
    binds = typeToUtype <$> Map.fromList
      [ ("Error", TyVar (MkVar "a"))
      , ("putint", TyFun intType intType)
      , ("getint", intType)
      , ("add", binopType intType intType intType)
      , ("sub", binopType intType intType intType)
      , ("mult", binopType intType intType intType)
      , ("div", binopType intType intType intType)
      , ("sll", binopType intType intType intType)
      , ("srl", binopType intType intType intType)
      , ("or", binopType intType intType intType)
      , ("xor", binopType intType intType intType)
      , ("not", TyFun intType intType)
      , ("and", binopType intType intType intType)
      , ("nand", binopType intType intType intType)
      , ("leq", binopType intType intType intType)
      , ("lt", binopType intType intType intType)
      ]
  } in MkIREnv
         { envVars = binds
         , envTyVars = Set.empty
         , envDataTys = Map.fromList
             [ ("Void", (TyData (MkName "Void") [],
                             [ Ctor (MkName "Error") []
                             ]))
             ]
         }

-- * Typechecking
-- Uses unification to infer and check types

type UType = UTerm TypeF IntVar
type UBindingState = IntBindingState TypeF
newtype Typecheck a =
  Typecheck { unTypecheck :: ReaderT IREnv (
                                  ExceptT TCError (
                                      IntBindingT TypeF Identity)) a }
  deriving ( Functor
           , Applicative
           , Monad
           , MonadReader IREnv
           , MonadError TCError)

-- | Represents possible typechecking errors
data TCError = TCOccursFailure IntVar UType
             | TCUnknownDataType String -- ^ Unknown TyData
             | TCUnknownTyVar String -- ^ Unknown type variable
             | TCUnknownVar String -- ^ Unknown syntax variable
             | TCUnknownDataCon String -- ^ Unknown constructor
             | TCDataConArgMismatch Int Int
             | TCArgCountMismatch Int Int
             | TCMismatch (TypeF UType) (TypeF UType)
             | TCNoBranch
             deriving (Show)

instance Fallible TypeF IntVar TCError where
  occursFailure = TCOccursFailure
  mismatchFailure = TCMismatch

mkMetaVar :: Typecheck UType
mkMetaVar = Typecheck . lift . lift $ UVar <$> freeVar

unifyTy :: UType -> UType -> Typecheck UType
unifyTy t1 t2 = Typecheck . lift $ t1 =:= t2

-- | [applyBindings] lifted to the [Typecheck] monad
forceBinding :: UType -> Typecheck UType
forceBinding = Typecheck . lift . applyBindings

-- | Instantiates free variables inside of a UType
instantiateType :: UType -> Typecheck UType
instantiateType t = do
  fvs <- asks (Set.toList . envFreeTyVars t)
  fvMap <- Map.fromList <$> traverse (\n -> (n, ) <$> mkMetaVar) fvs
  pure $ inst fvMap t
  where
    inst :: Map String UType -> UType -> UType
    inst vars = \case
      UTerm (TyVarF v) -> case Map.lookup (getVarName v) vars of
        Just b -> b
        Nothing -> UTerm (TyVarF v)
      UTerm (TyDataF n args) -> UTerm (TyDataF n (inst vars <$> args))
      UTerm (TyFunF arg res) -> UTerm (TyFunF (inst vars arg) (inst vars res))
      UVar x -> UVar x

inferType :: IsExpr r => r -> Typecheck UType
inferType expr = do
  exprTy <- mkMetaVar
  checkType (getExpr expr) exprTy

checkType :: IsExpr r => ExprF r -> UType -> Typecheck UType
checkType (ConstIntF _) t2 = unifyTy (UTerm (TyDataF (MkName "Int") [])) t2
checkType (VarF v) t2 = asks (envLookupVar (getVarName v)) >>= \case
  Just ty -> do
    instTy <- instantiateType ty
    unifyTy instTy t2
  Nothing -> throwError $ TCUnknownVar (getVarName v)
checkType (LetF x t1 t12) t2 = do
  tyVar <- inferType t1 >>= Typecheck . lift . applyBindings
  resTy <- local (updateEnvVars (Map.insert (getVarName x) tyVar)) (inferType t12)
  unifyTy resTy t2
checkType (AppF fun args) t2 = do
  fTy <- inferType fun >>= forceBinding
  argTys <- NE.toList <$> traverse inferType args
  case fTy of
    fty@(UTerm (TyFunF {})) ->
      let (fargs, fres) = utypeSplitFun fty
          (appArgs, unappArgs) = splitAt (length argTys) fargs in
      -- Handle partial evaluation here
      if null unappArgs
        then unifyTy (utypeMkFun argTys t2) fTy >> pure t2
        else do
          -- Partial evaluation, so the result type is actually a function
          -- that has the unapplied arguments as its arguments
          args' <- zipWithM unifyTy appArgs argTys
          let partialFun = utypeMkFun unappArgs fres
          unifyTy (utypeMkFun args' partialFun) t2
          pure partialFun
    _ -> unifyTy (utypeMkFun argTys t2) fTy >> pure t2
checkType (CaseF t1 brs def) t2 = do
  t1Ty <- inferType t1
  when (null brs && null def) $ throwError TCNoBranch
  brTys <- traverse (inferBranchType t1Ty) brs
  -- Check if all branches return the same type
  -- We use foldM here so the structure with most sharing is returned
  foldM unifyTy t2 brTys

inferBranchType :: IsExpr r => UType -> BranchF r -> Typecheck UType
inferBranchType argTy (BrInt _ t1) = do
  unifyTy argTy (UTerm intTypeF)
  inferType t1
inferBranchType argTy (BrCtor dcName args t1) =
  asks (envLookupDataCon (getName dcName)) >>= \case
    Just (dty, Ctor _ tys) -> do
      let dconArgCnt = length tys
      let argGotCnt = length args
      dataConTy <- instantiateType (typeToUtype dty)
      unifyTy dataConTy argTy
      when (dconArgCnt /= argGotCnt) $
        throwError (TCDataConArgMismatch dconArgCnt argGotCnt)
      -- TODO: Instantiate variables properly
      let instBindTys = typeToUtype <$> tys
      let newEnv = updateEnvVars (flip Map.union (Map.fromList $
                                                  (getVarName <$> args) `zip` instBindTys))
      local newEnv (inferType t1)
    Nothing -> throwError $ TCUnknownDataCon (getName dcName)

runTypecheck :: IREnv -> Typecheck a -> Either TCError a
runTypecheck env
  = runIdentity
  . evalIntBindingT
  . runExceptT
  . flip runReaderT env
  . unTypecheck

runTypeInference :: IsExpr r => r -> Either TCError UType
runTypeInference t = runTypecheck defaultEnv (inferType t >>= (Typecheck . lift . applyBindings))

-- | Updates the environment with the top level bindings
collectTopLevelBinds :: ProgramF r -> IREnv -> IREnv
collectTopLevelBinds (Prog _ decls) env = foldl' (flip f) env decls
  where
    f :: DeclF r -> IREnv -> IREnv
    f (DataTy name tvars ctors) =
      envInsertTyData (getName name) (TyData name (TyVar <$> tvars)) ctors
    f (Func name _ ty _ _) =
      updateEnvVars (Map.insert (getName name) (typeToUtype ty))

checkValidDataName :: String -> Typecheck ()
checkValidDataName name =
    asks (envLookupDataTy name) >>= \case
      Just _ -> pure ()
      Nothing -> throwError $ TCUnknownDataType name

checkValidType :: Type -> Typecheck ()
checkValidType = cata $ \case
  TyDataF name _ -> checkValidDataName (getName name)
  TyVarF name -> asks (envHasTyVar (getVarName name)) >>= \case
    True -> pure ()
    False -> throwError $ TCUnknownTyVar (getVarName name)
  TyFunF _ _ -> pure ()

typecheckProgram :: IsExpr r => ProgramF r -> Typecheck ()
typecheckProgram (Prog _ decls) = forM_ decls $ \case
  DataTy _ tyVars ctors ->
    local (envUpdateTyVars (`Set.union` Set.fromList (getVarName <$> tyVars))) $
      forM_ ctors $ \(Ctor _ args) ->
                      forM_ args checkValidType
  Func _ tyvars ty args body -> do
    let (argTys, assertTy) = tySplitFun ty
    let (argCntExpect, argCntGot) = (length argTys, length args)
    when (argCntExpect /= argCntGot) $
      throwError $ TCArgCountMismatch argCntExpect argCntGot
    let tyVarEnv = envUpdateTyVars (`Set.union` Set.fromList (getVarName <$> tyvars))
    let bindEnv = updateEnvVars (`Map.union`
                                    Map.fromList
                                    (zip (getVarName <$> args) (typeToUtype <$> argTys)))
    local (bindEnv . tyVarEnv) $ checkType (getExpr body) (typeToUtype assertTy)
    pure ()

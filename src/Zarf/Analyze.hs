{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Zarf.Analyze ( putBindings
                    , putIncludeBindings) where

import Data.Validation
import Data.List.NonEmpty as NEL
import Control.Monad.State
import Control.Monad.Except
import qualified Data.Map as Map

import qualified Zarf.IR as IR
import qualified Zarf.Ast as Ast
import Zarf.Common

type AnalyzeError = Validation (NonEmpty ZarfError)

-- | Throws a ZarfError if there are any errors in AnalyzeError, otherwise
-- | returns the successful values
collectErrors :: (ContextM m, ErrorM m) =>
                 (a -> m (AnalyzeError b)) ->
                 String ->
                 [a] ->
                 m [b]
collectErrors f fname items = do
  errors <- sequenceA <$> mapM f items
  case errors of
    Success res -> pure res
    Failure errs -> throwError (MkError (MkName fname) <$> NEL.toList errs)

-- | Repeatedly runs the action, accumulating errors in the AnalyzeError.
accumErrors :: (ContextM m) =>
               (a -> m (AnalyzeError b)) ->
               [a] ->
               m (AnalyzeError [b])
accumErrors f as = do
  res <- sequenceA (f <$> as)
  pure (sequenceA res)

putIncludeBindings :: (ContextM m, ErrorM m) => Ast.Program -> m ()
putIncludeBindings (Ast.Prog decls) = collectErrors insDecl fname decls *> pure ()
  where
    fname = "<include>"

    insDecl :: (ContextM m) => Ast.Decl -> m (AnalyzeError ())
    insDecl (Ast.Func name _ _ _ _) = attemptBind name (BCtor fname)
    insDecl (Ast.DataTy _ _ ctors) = (fmap (const ())) <$> accumErrors insCtor ctors

    insCtor :: (ContextM m) => Ast.Constructor -> m (AnalyzeError ())
    insCtor (Ast.Ctor name _) = attemptBind name (BCtor fname)

putBindings :: (ContextM m, ErrorM m) => IR.Program -> m ()
putBindings (IR.Prog fname decls) = collectErrors insDecl fname decls *> pure ()
  where
    insDecl :: (ContextM m) => IR.Decl -> m (AnalyzeError ())
    insDecl (IR.Func name _ _ _ _) = attemptBind name (BCtor fname)
    insDecl (IR.DataTy _ _ ctors) = (fmap (const ())) <$> accumErrors insCtor ctors

    insCtor :: (ContextM m) => IR.Constructor -> m (AnalyzeError ())
    insCtor (IR.Ctor name _) = attemptBind name (BCtor fname)

attemptBind :: (ContextM m) => Name -> BindingTy -> m (AnalyzeError ())
attemptBind name btype = do
  bindings <- get
  case Map.lookup (getName name) bindings of
    Just b -> pure $ Failure (RepeatedBinding name (getBindingFile b) :| [])
    Nothing -> do
      put (Map.insert (getName name) btype bindings)
      pure (Success ())

{-# LANGUAGE FlexibleContexts #-}

module Zarf.Translate where

import Control.Monad.State
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.List.NonEmpty as NE
import qualified Zarf.Ast as A
import qualified Zarf.IR as IR
import Zarf.Common

translateProg :: (ContextM m) => IR.Program -> m A.Program
translateProg (IR.Prog _ decls) = A.Prog <$> mapM translateDecl decls

-- | Translates the given declaration and adds it to the context.
translateDecl :: (ContextM m) => IR.Decl -> m A.Decl
translateDecl (IR.DataTy name typarams ctors) =
  pure $ A.DataTy name typarams (fmap translateCtor ctors)
translateDecl (IR.Func name tyvars typ args body) = do
  bindings <- get
  let bindings' = Map.insert (getName name) (BVar "<todo>") bindings
  put bindings'
  ast <- A.Func name tyvars (translateType typ) args <$> translateExpr body
  put bindings'
  pure ast

translateCtor :: IR.Constructor -> A.Constructor
translateCtor (IR.Ctor name params) = A.Ctor name (translateType <$> params)

translateExpr :: (ContextM m) => IR.Expr -> m A.Expr
translateExpr (IR.Var var) = do
  bindings <- get
  case Map.lookup (getVarName var) bindings of
    Just (BCtor _) -> (\(v, f) -> f (A.Result (A.ArgVar v))) <$> rebindVar var
    _ -> pure $ A.Result (A.ArgVar var)
translateExpr (IR.ConstInt x) = pure $ A.Result (A.ArgInt x)
translateExpr (IR.Let var value expr) = do
  modify (Map.insert (getVarName var) (BVar "<todo>"))
  ((vname, vexprf), vargs) <- case value of
    IR.App func args -> do
      (fname, hexprf) <- case func of
        -- We might end up redundantly rebinding a variable, so check here
        IR.Var n -> pure (n, id)
        -- Otherwise, it's some compound expression, so flatten it
        _ -> flattenApp func
      (argNames, exprsf) <- unzip <$> mapM flattenApp (NE.toList args)
      let resExprs = foldr (.) id (hexprf : exprsf)
      pure ((fname, resExprs), (A.ArgVar <$> argNames))
    v -> flip (,) [] <$> flattenApp v
  vexprf . A.Let var (A.ArgVar vname) vargs <$> translateExpr expr
translateExpr t@(IR.App _ _) = do
  (resName, exprf) <- flattenApp t
  pure $ exprf (A.Result (A.ArgVar resName))
translateExpr (IR.Case expr branches other) = do
  (resName, exprf) <- flattenApp expr
  branches' <- mapM translateBranch branches
  other' <- mapM translateExpr other
  pure $ exprf (A.Case (A.ArgVar resName) branches' other')

translateBranch :: (ContextM m) => IR.Branch -> m A.Branch
translateBranch (IR.BrCtor name args expr) = A.BrCtor name args <$> translateExpr expr
translateBranch (IR.BrInt x expr) = A.BrInt x <$> translateExpr expr

flattenApp :: (ContextM m) => IR.Expr -> m (Variable, A.Expr -> A.Expr)
flattenApp (IR.App func args) = do
  (fname, hexprf) <- case func of
    -- We might end up redundantly rebinding a variable, so check here
    IR.Var n -> pure (n, id)
    -- Otherwise, it's some compound expression, so flatten it
    _ -> flattenApp func
  (argNames, exprsf) <- NE.unzip <$> mapM flattenApp args
  let resExprs = foldr (.) id (hexprf : NE.toList exprsf)
  resName <- MkVar <$> genBinding "res"
  pure (resName, resExprs . A.Let resName (A.ArgVar fname) (fmap A.ArgVar (NE.toList argNames)))
flattenApp (IR.Var v) = do
  bindings <- get
  case Map.lookup (getVarName v) bindings of
    Just (BCtor _) -> rebindVar v
    _ -> pure (v, id)
flattenApp (IR.ConstInt x) = pure (MkVar (show x), id)
flattenApp (IR.Let _ _ _) = error "Nested let not handled"
flattenApp (IR.Case _ _ _) = error "Nested case not handled"
-- TODO: Handle nested case/let

rebindVar :: (ContextM m) => Variable -> m (Variable, A.Expr -> A.Expr)
rebindVar v = do
  freshN <- MkVar <$> genBinding "res"
  pure (freshN, A.Let freshN (A.ArgVar v) [])

genBinding :: (ContextM m) => String -> m String
genBinding prefix = do
  bindings <- get
  let b = fresh prefix bindings 0
  put (Map.insert b (BVar "<generated>") bindings)
  pure b

fresh :: String -> Map String BindingTy -> Int -> String
fresh prefix ctx count =
  let name = prefix ++ show count in
    case Map.lookup name ctx of
      Just _ -> fresh prefix ctx (count + 1)
      Nothing -> name

translateType :: IR.Type -> A.Type
translateType (IR.TyData name args) = A.TyData name (translateType <$> args)
translateType (IR.TyVar name) = A.TyVar name
translateType t@(IR.TyFun _ _) =
  let (args, res) = IR.tySplitFun t
  in A.TyFun (translateType <$> args) (translateType res)

module Zarf.Cli where

import Options.Applicative
import Data.List.NonEmpty
import Data.Semigroup ((<>))

data Command = CmdCompile { inputFiles :: NonEmpty String
                           , outputFile :: Maybe String
                           , squash :: Bool }

cmdParser :: ParserInfo Command
cmdParser = info compileCmd (fullDesc
                             <> progDesc "Syntax sugar for Zarf code")

compileCmd :: Parser Command
compileCmd = CmdCompile
           <$> ((:|) <$> inputFile <*> many inputFile)
           <*> (optional $ strOption (long "out" <> short 'o' <> metavar "out" <> help "Output file"))
           <*> (switch $ long "squash" <> short 's' <> help "Bundle all include files in output")
  where inputFile = strArgument (metavar "<files...>" <> help "Input source files")

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveFunctor, DeriveFoldable, DeriveTraversable #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}

{-|
Defines the high-level language used for the intermediate representation.
-}
module Zarf.IR where

import qualified Control.Unification as Uni
import Text.Megaparsec.Pos (SourcePos)
import Data.Functor.Compose (Compose(..))
import Data.Functor.Foldable (Fix(..), unfix, cata)
import Data.Eq.Deriving (deriveEq1)
import Text.Show.Deriving (deriveShow1)
import Data.List.NonEmpty (NonEmpty(..))
import Zarf.Common

-- * Expressions

data ExprF r = LetF Variable r r
             | CaseF r [BranchF r] (Maybe r)
             | VarF Variable
             | ConstIntF Int
             | AppF r (NonEmpty r)
             deriving (Functor, Foldable, Traversable)

data BranchF r = BrCtor Name [Variable] r
               | BrInt Int r
               deriving (Functor, Foldable, Traversable)

type Expr = Fix ExprF
type Branch = BranchF Expr

-- Patterns for Expr (to avoid breaking existing code)
pattern Let :: Variable -> Expr -> Expr -> Expr
pattern Let x t1 t2 = Fix (LetF x t1 t2)
pattern Case :: Expr -> [Branch] -> Maybe Expr -> Expr
pattern Case t1 alts def = Fix (CaseF t1 alts def)
pattern Var :: Variable -> Expr
pattern Var x = Fix (VarF x)
pattern ConstInt :: Int -> Expr
pattern ConstInt x = Fix (ConstIntF x)
pattern App :: Expr -> NonEmpty Expr -> Expr
pattern App f args = Fix (AppF f args)

{-# COMPLETE Let, Case, Var, ConstInt, App #-}

-- | Returns the expression in a branch
branchExpr :: BranchF r -> r
branchExpr (BrCtor _ _ r) = r
branchExpr (BrInt _ r) = r

-- * Types

-- | Explicit fix point for Type, mainly for unification
data TypeF r = TyDataF Name [r]
             | TyVarF Variable
             | TyFunF r r
             deriving (Functor, Foldable, Traversable)

-- | Represents a type. Prefer 'Type' over 'TypeF' when possible.
type Type = Fix TypeF

intType :: Type
intType = TyData (MkName "Int") []

binopType :: Type -> Type -> Type -> Type
binopType t1 t2 tres = TyFun t1 (TyFun t2 tres)

intTypeF :: TypeF a
intTypeF = TyDataF (MkName "Int") []

pattern TyData :: Name -> [Type] -> Type
pattern TyData n args = Fix (TyDataF n args)
pattern TyVar :: Variable -> Type
pattern TyVar v = Fix (TyVarF v)
pattern TyFun :: Type -> Type -> Type
pattern TyFun arg res = Fix (TyFunF arg res)

{-# COMPLETE TyData, TyVar, TyFun #-}

instance Uni.Unifiable TypeF where
  zipMatch (TyVarF v1) (TyVarF v2) =
    if v1 == v2 then Just (TyVarF v1) else Nothing
  zipMatch (TyDataF n1 vs1) (TyDataF n2 vs2) =
    if n1 == n2 && length vs1 == length vs2
      then Just $ TyDataF n1 (zipWith (curry Right) vs1 vs2)
      else Nothing
  zipMatch (TyFunF as1 r1) (TyFunF as2 r2) =
    Just $ TyFunF (Right (as1, as2)) (Right (r1, r2))
  zipMatch _ _ = Nothing

-- | Splits a function type into argument and result. If the given type is not a
-- function, the returned argument list will be empty.
tySplitFun :: Type -> ([Type], Type)
tySplitFun (TyFun arg res) =
  let (args, res') = tySplitFun res in (arg : args, res')
tySplitFun t = ([], t)

-- | Constructs a function type using the given argument types and result type.
tyMkFun :: [Type] -> Type -> Type
tyMkFun args res = foldr TyFun res args

-- Derive instances here because of mutual recursion
deriveEq1 ''ExprF
deriveShow1 ''ExprF
deriveEq1 ''BranchF
deriveShow1 ''BranchF
deriveEq1 ''TypeF
deriveShow1 ''TypeF
deriving instance (Show r) => Show (ExprF r)
deriving instance (Eq r) => Eq (ExprF r)
deriving instance (Show r) => Show (BranchF r)
deriving instance (Eq r) => Eq (BranchF r)
deriving instance (Show r) => Show (TypeF r)
deriving instance (Eq r) => Eq (TypeF r)

-- * Program

data DeclF r = DataTy Name [Variable] [Constructor]
             | Func Name [Variable] Type [Variable] r
             deriving (Eq, Show, Functor)

type Decl = DeclF Expr

data ProgramF r = Prog String [DeclF r] deriving (Eq, Show, Functor)

type Program = ProgramF Expr

data Constructor = Ctor Name [Type] deriving (Eq, Show)

-- | Given the constructor's data type, returns the type of the constructor
-- function.
getCtorType :: Constructor -> Type -> Type
getCtorType (Ctor _ args) = foldr (\a -> \f -> TyFun a . f) id args

-- * AST annotations
--
-- These should be constructed by using 'AnnExprF'
--
-- @
--   type TSExprF = AnnExprF (Type, SourcePos)
--   type TSExpr = Fix TSExprF
--   instance HasType (TSExprF r) where
--     getType = fst . annExprAnn . getCompose
--   instance HasSource (TSExprF r) where
--     getSource = snd . annExprAnn . getCompose
-- @
--

-- | An AST with an annotation
newtype AnnExprF a r = AnnExprF (a, r)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

annExprAnn :: AnnExprF a r -> a
annExprAnn (AnnExprF (a, _)) = a

annExprRec :: AnnExprF a r -> r
annExprRec (AnnExprF (_, r)) = r

type AnnExpr a = Fix (Compose (AnnExprF a) ExprF)

mkAnnExpr :: a -> ExprF (AnnExpr a) -> AnnExpr a
mkAnnExpr ann expr = Fix . Compose $ AnnExprF (ann, expr)

-- | Strips annotations, returning just the expression AST
unannotate :: AnnExpr ann -> Expr
unannotate = cata (Fix . annExprRec . getCompose)

-- | An AST with type information
type TypedExprF = AnnExprF Type
type TypedExpr = AnnExpr Type

-- | An AST with source information
type SourcedExprF = AnnExprF SourcePos
type SourcedExpr = AnnExpr SourcePos

-- * Typeclasses

-- | An AST that represents an expression
class IsExpr t where
  getExpr :: t -> ExprF t

instance IsExpr Expr where
  getExpr = unfix

instance IsExpr (AnnExpr ann) where
  getExpr = annExprRec . getCompose . unfix

-- | An AST that has type information
class HasType a where
  getType :: a -> Type

instance HasType (TypedExprF r) where
  getType = annExprAnn

instance HasType (f (g r)) => HasType (Compose f g r) where
  getType = getType . getCompose

instance HasType TypedExpr where
  getType = getType . unfix

-- | An AST that has source information
class HasSource a where
  getSource :: a -> SourcePos

instance HasSource (SourcedExprF r) where
  getSource = annExprAnn

instance HasSource (f (g r)) => HasSource (Compose f g r) where
  getSource = getSource . getCompose

instance HasSource SourcedExpr where
  getSource = getSource . unfix

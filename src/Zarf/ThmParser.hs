module Zarf.ThmParser where

import Data.Void
import Data.List.NonEmpty
import Text.Megaparsec
import qualified Text.Megaparsec.Char as P
import qualified Text.Megaparsec.Char.Lexer as L

import Zarf.Parser (digit, upper, alphaNum, integer, var, identifier, parens, brackets)
import Zarf.Common
import Zarf.Ast

type ParserT m a = ParsecT Void String m a

sc :: ParserT m ()
sc = L.space P.space1 lineComment blockComment
  where
    lineComment = L.skipLineComment "#"
    blockComment = L.skipBlockComment "\"\"\"" "\"\"\""

lexeme :: ParserT m a -> ParserT m a
lexeme = L.lexeme sc

symbol :: String -> ParserT m String
symbol = L.symbol sc

keyword :: String -> ParserT m String
keyword word = lexeme $ P.string word <* notFollowedBy alphaNum

program :: ParserT m Program
program = Prog <$> (sc *> many decl <* eof)

decl :: ParserT m Decl
decl = dataType <|> function

dataType :: ParserT m Decl
dataType = do
  lexeme dataTypeKW
  name <- MkName <$> lexeme typeName
  params <- option [] (brackets typeVarList)
  symbol "="
  ctors <- constructor `sepBy1` (symbol "|")
  pure $ DataTy name params ctors

constructor :: ParserT m Constructor
constructor = do
  name <- MkName <$> lexeme typeName
  args <- many (lexeme type')
  pure $ Ctor name args

function :: ParserT m Decl
function = do
  (name, tvars, typ) <- funcDecl
  (name', vars, expr) <- funcDef
  if name == name'
    then pure $ Func name tvars typ vars expr
    else fail $ "Function declared as " ++ show (getName name)
                ++ ", but binding declared as " ++ show (getName name')

funcDecl :: ParserT m (Name, [Variable], Type)
funcDecl = do
  lexeme funcKW
  name <- MkName <$> lexeme identifier
  tvl <- option [] (brackets typeVarList)
  symbol "::"
  typ <- type'
  pure (name, tvl, typ)

funcDef :: ParserT m (Name, [Variable], Expr)
funcDef = do
  lexeme funcKW
  name <- MkName <$> lexeme identifier
  vl <- maybe [] toList <$> optional varList
  symbol "="
  e <- lexeme expression
  pure $ (name, vl, e)

expression :: ParserT m Expr
expression = let' <|> case' <|> result <?> "expression"
  where
    let' = do
      keyword "let"
      v <- lexeme var
      symbol "="
      ident <- lexeme (ArgVar . MkVar <$> (identifier <|> typeName))
      args <- many (lexeme arg)
      keyword "in"
      e <- lexeme expression
      pure $ Let v ident args e
    case' = do
      keyword "case"
      v <- lexeme var
      keyword "of"
      symbol "{"
      branches <- many (lexeme branch)
      elseBr <- optional (lexeme elseBranch)
      symbol "}"
      pure $ Case (ArgVar v) branches elseBr
    result = Result <$> arg

branch :: ParserT m Branch
branch = litBranch <|> consBranch <?> "branch"
  where
    litBranch = do
      i <- lexeme integer
      symbol "=>"
      e <- lexeme expression
      pure $ BrInt i e
    consBranch = do
      n <- MkName <$> lexeme typeName
      vs <- many (lexeme var)
      symbol "=>"
      e <- lexeme expression
      pure $ BrCtor n vs e

elseBranch :: ParserT m Expr
elseBranch = symbol "_" >> symbol "=>" >> lexeme expression

type' :: ParserT m Type
type' = try typeFun <|> typeData <|> (TyVar <$> lexeme typeVar)

typeData :: ParserT m Type
typeData = do
  name <- MkName <$> lexeme typeName
  params <- lexeme typeParams
  pure (TyData name params)

typeList :: ParserT m [Type]
typeList = (lexeme type') `sepBy1` (symbol ",")

typeParams :: ParserT m [Type]
typeParams = option [] (brackets typeList)

typeFun :: ParserT m Type
typeFun = do
  params <- parens typeList
  symbol "->"
  resTy <- type'
  pure $ TyFun params resTy

varList :: ParserT m (NonEmpty Variable)
varList = (:|) <$> lexeme var <*> many (lexeme var)

typeVarList :: ParserT m [Variable]
typeVarList = sepBy1 (lexeme typeVar) (symbol ",")

funcKW :: ParserT m String
funcKW = choice [keyword "function", keyword "func", keyword "fun"]

dataTypeKW :: ParserT m String
dataTypeKW = choice [keyword "datatype", keyword "data", keyword "type"]

typeName :: ParserT m String
typeName = ((:) <$> upper <*>
                       many (alphaNum <|> P.char '_' <|> digit) <?> "typeName")

arg :: ParserT m Arg
arg = (ArgInt <$> integer) <|> (ArgVar <$> var) <?> "arg"

hex :: ParserT m Int
hex = P.char '0' >> P.char 'x' >> L.hexadecimal

typeVar :: ParserT m Variable
typeVar = MkVar <$> identifier <?> "typeVar"

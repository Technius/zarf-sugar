{-# LANGUAGE OverloadedStrings #-}
{-|
Defines the abstract syntax of Zarf.
-}
module Zarf.Ast (
    Program(..)
  , Decl(..)
  , Constructor(..)
  , Expr(..)
  , Branch(..)
  , Arg(..)
  , Type(..)
  , intType) where

import Data.Text.Prettyprint.Doc
import Data.Semigroup ((<>), Semigroup)
import Zarf.Common

-- * AST types

data Program = Prog [Decl] deriving (Eq, Show)

data Decl = DataTy Name [Variable] [Constructor]
          | Func Name [Variable] Type [Variable] Expr
          deriving (Eq, Show)

data Constructor = Ctor Name [Type] deriving (Eq, Show)

data Expr = Let Variable Arg [Arg] Expr
          | Case Arg [Branch] (Maybe Expr)
          | Result Arg
          deriving (Eq, Show)

data Branch = BrCtor Name [Variable] Expr
            | BrInt Int Expr
            deriving (Eq, Show)

data Arg = ArgInt Int
         | ArgVar Variable
         deriving (Eq, Show)

data Type = TyData Name [Type]
          | TyVar Variable
          | TyFun [Type] Type
          deriving (Eq, Show)

intType :: Type
intType = TyData (MkName "Int") []

instance Semigroup Program where
  (Prog decls1) <> (Prog decls2) = Prog (decls1 ++ decls2)

instance Monoid Program where
  mempty = Prog []
  mappend = (<>)

-- * Pretty printing

instance Pretty Program where
  pretty (Prog decls) = vsep (fmap pretty decls)

-- | Optionally displays type parameters
optTypeParams :: (Pretty a) => [a] -> Maybe (Doc ann)
optTypeParams args =
  if length args > 0
    then Just (group $ encloseSep "[" "]" ", " (fmap pretty args))
    else Nothing

instance Pretty Decl where
  pretty (DataTy name args ctors) =
    hsep (["data", pretty name] ++ maybe [] pure (optTypeParams args))
    <> (group . align . encloseSep " = " emptyDoc " | " $ fmap pretty ctors)
  pretty (Func name _ typ args body) = decl <> hardline <> def
    where
      decl = hsep $
        ["fun", pretty name] ++
        ["::", pretty typ]
      def = nest 2 $
        hsep (["fun", pretty name] ++ fmap pretty args) <+> "="
        <> group (line <> pretty body)

instance Pretty Constructor where
  pretty (Ctor name args) = hsep (pretty name : fmap pretty args)

instance Pretty Expr where
  pretty (Let var fun args expr) =
    align $ "let" <+> pretty var <+> "=" <+> hsep (pretty fun : fmap pretty args) <+> "in"
    <> line <> pretty expr
  pretty (Case arg branches elseBr) =
    "case" <+> pretty arg <+> "of {" <> line
    <> (indent 2 (layout br))
    <> line <> "}"
    where layout bs = concatWith (\x y -> x <> hardline <> y) bs
          br = fmap pretty branches ++ elseBr'
          elseBr' = case elseBr of
            Just e -> ["_ =>" <> softline <> pretty e]
            Nothing -> []
  pretty (Result arg) = pretty arg

instance Pretty Branch where
  pretty (BrCtor n vars expr) =
    hang 2 $ hsep (pretty n : fmap pretty vars) <+> "=>" <> group (line <> pretty expr)
  pretty (BrInt x expr) = pretty x <+> "=>" <> softline <> pretty expr

instance Pretty Arg where
  pretty (ArgInt x) = pretty x
  pretty (ArgVar v) = pretty v

instance Pretty Type where
  pretty (TyData name params) = mconcat $ [pretty name] ++ maybe [] pure (optTypeParams params)
  pretty (TyVar var) = pretty var
  pretty (TyFun typ res) =
    group . align $ encloseSep "(" ")" ", " (fmap pretty typ) <+> "->" <+> pretty res

module Zarf.Parser where

import Control.Applicative (liftA2)
import Data.Void
import Data.List.NonEmpty (NonEmpty(..))
import Text.Megaparsec
import Text.Megaparsec.Expr
import qualified Data.List.NonEmpty as NEL
import qualified Text.Megaparsec.Char as P
import qualified Control.Applicative.Combinators.NonEmpty as PNE
import qualified Text.Megaparsec.Char.Lexer as L

import Zarf.IR
import Zarf.Common

type ParserT m a = ParsecT Void String m a

sc :: ParserT m ()
sc = L.space P.space1 lineComment blockComment
  where
    lineComment = L.skipLineComment "#"
    blockComment = L.skipBlockComment "\"\"\"" "\"\"\""

lexeme :: ParserT m a -> ParserT m a
lexeme = L.lexeme sc

symbol :: String -> ParserT m String
symbol = L.symbol sc

parens :: ParserT m a -> ParserT m a
parens = between (symbol "(") (symbol ")")

brackets :: ParserT m a -> ParserT m a
brackets = between (symbol "[") (symbol "]")

reservedKeywords :: [String]
reservedKeywords = ["fun", "func", "function", "data", "type", "datatype", "let",
                    "in", "case", "of"]

keyword :: String -> ParserT m String
keyword word = lexeme $ P.string word <* notFollowedBy alphaNum

program :: String -> ParserT m (ProgramF SourcedExpr)
program fname = Prog fname <$> (sc *> many decl <* eof)

decl :: ParserT m (DeclF SourcedExpr)
decl = dataType <|> function

dataType :: ParserT m (DeclF SourcedExpr)
dataType = do
  lexeme dataTypeKW
  name <- lexeme typeName
  params <- option [] (brackets typeVarList)
  symbol "="
  ctors <- constructor `sepBy1` (symbol "|")
  pure $ DataTy name params ctors

constructor :: ParserT m Constructor
constructor = do
  name <- lexeme typeName
  args <- many type'
  pure $ Ctor name args

function :: ParserT m (DeclF SourcedExpr)
function = do
  (name, tvars, typ) <- funcDecl
  (name', vars, expr) <- funcDef
  if name == name'
    then pure $ Func name tvars typ vars expr
    else fail $ "Function declared as " ++ show (getName name)
                ++ ", but binding declared as " ++ show (getName name')

funcDecl :: ParserT m (Name, [Variable], Type)
funcDecl = do
  lexeme funcKW
  name <- MkName <$> lexeme identifier
  tvl <- option [] (brackets typeVarList)
  symbol "::"
  typ <- try typeFun <|> type'
  pure (name, tvl, typ)

funcDef :: ParserT m (Name, [Variable], SourcedExpr)
funcDef = do
  lexeme funcKW
  name <- MkName <$> lexeme identifier
  vl <- option [] (NEL.toList <$> varList)
  symbol "="
  e <- lexeme expression
  pure $ (name, vl, e)

binOps :: [[(String, String)]]
binOps = [ [ ("*", "mult")
           , ("/", "div")
           , ("%", "mod") ]
         , [ ("+", "add")
           , ("-", "sub") ]
         , [ ("<<", "sll") ]
         , [ ("&", "and")
           , ("|", "or")
           , ("^", "xor") ]
         , [ ("==", "eq")
           , ("<=", "lte")
           , ("<", "lt") ] ]

opTable :: [[Operator (ParsecT Void String m) SourcedExpr]]
opTable = fmap binOp <$> binOps
 where
   binOp (symb, opName) = InfixL $ do
     -- At this point, we have just parsed the left expression
     opPos <- getPosition
     symbol symb
     let opVar = mkAnnExpr opPos (VarF $ MkVar opName)
     pure (\lhs rhs -> mkAnnExpr (getSource lhs) $ AppF opVar (lhs :| [rhs]))

tagSourcePos :: ParserT m (ExprF SourcedExpr) -> ParserT m SourcedExpr
tagSourcePos = liftA2 mkAnnExpr getPosition

expression :: ParserT m SourcedExpr
expression = exprLet <|> exprCase <|> exprApp <?> "expression"

exprVar :: ParserT m SourcedExpr
exprVar = tagSourcePos $ VarF <$> lexeme (var <|> (MkVar . getName <$> typeName))

exprConstInt ::  ParserT m SourcedExpr
exprConstInt = tagSourcePos $ ConstIntF <$> lexeme integer

exprApp :: ParserT m SourcedExpr
exprApp = makeExprParser term opTable
  where
    term = parens exprApp <|> try appExpr <|> exprVar <|> exprConstInt
    appExpr = tagSourcePos $ do
      let fParens = parens expression
      let fVar = tagSourcePos $ VarF . MkVar <$> (identifier <|> (getName <$> typeName))
      let arg = lexeme fVar <|> exprConstInt <|> lexeme fParens
      fun <- arg
      args <- PNE.some arg
      pure $ AppF fun args

exprLet :: ParserT m SourcedExpr
exprLet = do
  keyword "let"
  bindings <- ((pure <$> letOne) <|> letMany)
  keyword "in"
  e <- lexeme expression
  pure $ foldr ($) e bindings
  where letMany = between (symbol "{") (symbol "}") (letOne `sepBy1` (symbol ","))
        letOne = do
          pos <- getPosition
          v <- lexeme var
          symbol "="
          value <- exprApp
          pure (mkAnnExpr pos . LetF v value )

exprCase :: ParserT m SourcedExpr
exprCase = tagSourcePos $ do
  keyword "case"
  expr <- try (exprVar <* notFollowedBy exprVar) <|> expression
  keyword "of"
  symbol "{"
  branches <- (lexeme branch) `sepEndBy` (symbol ",")
  elseBr <- optional (lexeme elseBranch)
  symbol "}"
  pure $ CaseF expr branches elseBr

branch :: ParserT m (BranchF SourcedExpr)
branch = litBranch <|> consBranch <?> "branch"
  where
    litBranch = do
      i <- lexeme integer
      symbol "=>"
      e <- lexeme expression
      pure $ BrInt i e
    consBranch = do
      n <- lexeme typeName
      vs <- many (lexeme var)
      symbol "=>"
      e <- lexeme expression
      pure $ BrCtor n vs e

elseBranch :: ParserT m SourcedExpr
elseBranch = symbol "_" >> symbol "=>" >> lexeme expression

type' :: ParserT m Type
type' = typeData <|> (TyVar <$> lexeme typeVar) <|> parens typeFun

typeData :: ParserT m Type
typeData = label "typeData" $ do
  n <- lexeme typeName
  args <- option [] (brackets typeList)
  pure (TyData n args)

typeFun :: ParserT m Type
typeFun = do
  argTy <- type'
  symbol "->"
  resType <- lexeme (try typeFun <|> type')
  pure (TyFun argTy resType)

varList :: ParserT m (NonEmpty Variable)
varList = (:|) <$> lexeme var <*> many (lexeme var)

typeVarList :: ParserT m [Variable]
typeVarList = sepBy1 (lexeme typeVar) (symbol ",")

typeList :: ParserT m [Type]
typeList = sepBy1 (lexeme (try typeFun <|> type')) (symbol ",")

funcKW :: ParserT m String
funcKW = keyword "fun" <|> keyword "func" <|> keyword "function"

dataTypeKW :: ParserT m String
dataTypeKW = keyword "data" <|> keyword "type" <|> keyword "datatype"

identifier :: ParserT m String
identifier = try (ident >>= check)
  where
    ident = (:) <$> (lower <|> P.char '_') <*> many (alphaNum <|> P.char '_' <|> P.char '\'')
    check i = if i `elem` reservedKeywords
                 then fail $ show i ++ " is a reserved keyword"
                 else pure i

typeName :: ParserT m Name
typeName = MkName <$> ((:) <$> upper <*>
                       many (alphaNum <|> P.char '_' <|> digit) <?> "typeName")

typeVar :: ParserT m Variable
typeVar = MkVar <$> identifier <?> "typeVar"

var :: ParserT m Variable
var = MkVar <$> identifier <?> "variable"

integer :: ParserT m Int
integer = try hex <|> L.decimal <?> "integer"

hex :: ParserT m Int
hex = P.char '0' >> P.char 'x' >> L.hexadecimal

alphaNum :: ParserT m Char
alphaNum = (lower <|> upper <|> digit) <?> "alphaNum"

digit :: ParserT m Char
digit = P.digitChar <?> "digit"

upper :: ParserT m Char
upper = P.upperChar <?> "upper"

lower :: ParserT m Char
lower = P.lowerChar <?> "lower"

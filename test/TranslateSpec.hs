module TranslateSpec where

import Control.Monad.State
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.Map as Map
import Test.Tasty
import Test.Tasty.HUnit
import Zarf.IR as IR
import Zarf.Ast as A
import Zarf.Common
import Zarf.Translate

tests :: TestTree
tests = testGroup "Translate Tests"
  [ fvTests ]

runTranslateTest :: IR.Program -> (A.Program, Context)
runTranslateTest input =
  runState (translateProg input) (Map.fromList [])

fvTests :: TestTree
fvTests = testGroup "Free variable tests"
  [ fvTest1 ]

fvTest1 :: TestTree
fvTest1 = testCase "Repeated binding" $
            assertEqual "" output (fst $ runTranslateTest input)
  where
    x = MkVar "res0"
    y = MkVar "res1"
    input = IR.Prog ""
      [ IR.Func (MkName "foo") [] IR.intType [] $
          IR.Let x (IR.ConstInt 1) $
            IR.App (IR.Var (MkVar "plus")) (IR.Var x :| [IR.ConstInt 1])
      ]
    output = A.Prog
      [ A.Func (MkName "foo") [] A.intType [] $
          A.Let x (A.ArgInt 1) [] $
            A.Let y (A.ArgVar $ MkVar "plus") [A.ArgVar x, A.ArgInt 1] (A.Result (A.ArgVar y))
      ]

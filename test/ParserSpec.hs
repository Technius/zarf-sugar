module ParserSpec where

import Control.Monad (join)
import Data.Functor.Identity
import Data.List.NonEmpty (NonEmpty(..))
import Test.Tasty
import Test.Tasty.HUnit
import Text.Megaparsec
import qualified Zarf.Parser as P
import Zarf.IR
import Zarf.Common

testCaseParser :: (Show a, Eq a) => P.ParserT Identity a -> String -> a -> TestTree
testCaseParser parser input expected =
  testCase input $ assertEqual "" (Right expected) (runParser parser "" input)

tests :: TestTree
tests = testGroup "Parser Tests"
  [ typeTests
  , arithTests
  ]

typeTests :: TestTree
typeTests = testGroup "Type Parser"
  [ binopTests
  , higherOrderTests
  , invalidTests
  ]

binopTests :: TestTree
binopTests = testGroup "binop" $
  [ testCaseParser P.typeFun "a -> a -> a"
  , testCaseParser P.typeFun "a -> (a -> a)"
  ] <*> [binopTy]
  where
    varA = TyVar (MkVar "a")
    binopTy = TyFun varA (TyFun varA varA)

higherOrderTests :: TestTree
higherOrderTests = testGroup "higher order"
  [ testCaseParser P.typeFun "(a -> b) -> List[a] -> List[b]" mapTy
  , testCaseParser P.typeFun "(b -> a -> b) -> b -> List[a] -> b" foldlTy
  ]
  where
    var = TyVar . MkVar
    list' x = TyData (MkName "List") [var x]
    mapTy = tyMkFun [tyMkFun [var "a"] (var "b"), list' "a"] (list' "b")
    foldlTy =
      tyMkFun [tyMkFun [var "b", var "a"] (var "b"), var "b", list' "a"] (var "b")

invalidTests :: TestTree
invalidTests = testGroup "invalid"
  [ testInvalid P.typeFun "a ->"
  , testInvalid P.typeFun "->"
  , testInvalid P.typeFun "-> a"
  , testInvalid P.typeFun "(a -> b) -> c)"
  , testInvalid P.typeFun "a -> b) -> c"
  ]
  where testInvalid p input = testCase input $
          case runParser p "" input of
            Left _ -> pure ()
            Right x -> assertFailure ("Parsed as " ++ show x)

arithTests :: TestTree
arithTests = testGroup "Arithmetic Expressions" (fmap test (join P.binOps))
  where
    test (op, name) =
      testCaseParser (unannotate <$> P.expression) ("let x' = x " ++ op ++ " x in x'") (binOp name)
    x = MkVar ("x")
    x' = MkVar ("x'")
    binOp name =
      Let x' (App (Var (MkVar (name))) (Var x :| [Var x])) (Var x')

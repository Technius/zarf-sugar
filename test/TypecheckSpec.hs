module TypecheckSpec where

import Data.List.NonEmpty(NonEmpty(..))
import Test.Tasty
import Test.Tasty.HUnit
import Zarf.IR
import Zarf.Common
import Zarf.Typing

assertType :: Type -> Expr -> Assertion
assertType expect expr = case runTypeInference expr of
  Left err -> assertFailure $ "Typecheck error: " ++ show err
  Right utype ->
    assertEqual "" (Just expect) (utypeToType utype)

tests :: TestTree
tests = testGroup "Typecheck Tests"
  [ monoTests
  , exprTests
  ]

monoTests :: TestTree
monoTests = testGroup "Monomorphic typecheck tests"
  [ plusTest ]

plusTest :: TestTree
plusTest = testCase "plus application" $
  assertType intType (App (Var (MkVar "plus")) (ConstInt 5 :| [ConstInt 10]))

exprTests :: TestTree
exprTests = testGroup "Expression tests"
  [ letTest
  , caseIntTest
  ]

letTest :: TestTree
letTest = testCase "let binding" $
  assertType intType (Let (MkVar "x") (ConstInt 5) (Var (MkVar "x")))

caseIntTest :: TestTree
caseIntTest = testCase "int case binding" $
  assertType intType (Case (ConstInt 5)
                      [ BrInt 5 (ConstInt 10)
                      ] Nothing)

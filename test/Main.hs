module Main where

import Test.Tasty

import qualified ParserSpec
import qualified TranslateSpec
import qualified TypecheckSpec

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "All Tests"
  [ ParserSpec.tests
  , TypecheckSpec.tests
  , TranslateSpec.tests ]
